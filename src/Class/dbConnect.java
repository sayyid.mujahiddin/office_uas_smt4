/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
/**
 *
 * @author Sayyid
 */
public class dbConnect {  
    Connection cnn;
    Statement stmt;
    ResultSet rs;

    public dbConnect(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch(ClassNotFoundException e){
            JOptionPane.showMessageDialog(null,"error inisialisasi Class : "+ e);
        }
        try{
            stmt = DriverManager.getConnection("jdbc:mysql://localhost/kantor",
            "root",
            "").createStatement();
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,"error koneksi ke database : "+ e);
        }
    }
    
    public ResultSet lihatData(String sql){
        try{
            rs = stmt.executeQuery(sql);
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,"Error Query : "+ e);
        }
        return rs;
    }
    
    public void dmlData(String sql){
        try{
            stmt.executeUpdate(sql);
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error Query :"+ e);
        }
    }

    public static void main(String [] args){
        try{
            dbConnect cn = new dbConnect();
            JOptionPane.showMessageDialog(null,"Koneksi Sukses");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"Koneksi Gagal"+ e);
        }
    }
} // end dbConnect