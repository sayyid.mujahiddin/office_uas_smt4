/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Class;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
/**
 *
 * @author Sayyid
 */
public class Employee {
        static dbConnect cn = new dbConnect();
	static String table = "employees";
        
        public String NIP,name,division,dob,gender,religion,phone,address; 
        public int id;
        public Employee(int id,
            String NIP,
            String name,
            String division,
            String dob,
            String gender,
            String religion,
            String phone,
            String address
        ){}
                
	public static List<Employee> fetchToList(){
            List employeeList = new ArrayList<Employee>();
            try{
                ResultSet rows = fetch();
                //Employee[] arrEmployee;
                
                while(rows.next()){
                    Employee employee = (new Employee(
                        rows.getInt("id"),
                        rows.getString("NIP"),
                        rows.getString("name"),
                        rows.getString("division"),
                        rows.getString("dob"),
                        rows.getString("gender"),
                        rows.getString("religion"),
                        rows.getString("phone"),
                        rows.getString("address")
                    ));
                    employeeList.add(employee);
                    System.out.print(employee);
                }
//                System.out.print(employeeList);
                return employeeList;
            }
            catch(SQLException e){ 
                System.out.print(e);
            }
            return employeeList;
	}
        
        public static ResultSet fetch(){
            String sql = "select * from " + table;
            return cn.lihatData(sql);
	}
        
        public static boolean insert(
            String NIP,
            String name,
            String division,
            String dob,
            String gender,
            String religion,
            String phone,
            String address
        ){
            if(
                    NIP.length() < 1
                    || name.length() < 1
                    || division.length() < 1
                    || dob.length() < 1
                    || gender.length() < 1
                    || religion.length() < 1
                    || phone.length() < 1
                    || address.length() < 1
            ){
                System.out.print("field is empty");
                return false;
            }else{
                String sql="insert into "
                        + "employees(NIP, name, division, dob, gender, religion, phone, address) "
                        + "values("
                        + "'"+NIP+"',"
                        + "'"+name+"',"
                        + "'"+division+"',"
                        + "'"+dob+"',"
                        + "'"+gender+"',"
                        + "'"+religion+"',"
                        + "'"+phone+"',"
                        + "'"+address+"'"
                        + ")";
                System.out.println("SQL : "+sql);
                cn.dmlData(sql);
                return true;
            }
        }
        
        public static void update(
            String oldNIP,
            String NIP,
            String name,
            String division,
            String dob,
            String gender,
            String religion,
            String phone,
            String address){
            if(
                    NIP.length() < 1
                    || name.length() < 1
                    || division.length() < 1
                    || dob.length() < 1
                    || gender.length() < 1
                    || religion.length() < 1
                    || phone.length() < 1
                    || address.length() < 1
            ){
                System.out.print("field is empty");
            }else{
                String sql="update employees "
                        + "set "
                        + "NIP = '"+NIP+"',"
                        + "name = '"+name+"',"
                        + "division = '"+division+"',"
                        + "dob = '"+dob+"',"
                        + "gender = '"+gender+"',"
                        + "religion = '"+religion+"',"
                        + "phone = '"+phone+"',"
                        + "address = '"+address+"'"
                        + "where NIP = '"+ oldNIP + "'";
                System.out.println("SQL : "+sql);
                cn.dmlData(sql);
            }
        }
        
        public static void delete(String NIP){
            if(NIP.length() < 1){
                System.out.print("field is empty");
            }else{
                String sql = "delete from employees where NIP = '"+NIP+"'";
                cn.dmlData(sql);
            }
        }
}
